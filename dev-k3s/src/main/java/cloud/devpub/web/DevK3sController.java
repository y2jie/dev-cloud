package cloud.devpub.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : y1
 * @className : DevK3sController
 * @date: 2023/12/20 14:50
 * @description :
 */
@RestController
public class DevK3sController {
    private final static String VERSION = "1.0.0";

    @RequestMapping("/k3s")
    public String k3s() {
        return "k3s-pod-version:" + VERSION;
    }
}
