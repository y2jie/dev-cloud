package cloud.devpub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : y1
 * @className : cloud.devpub.DevK3sApplication
 * @date: 2023/12/20 14:49
 * @description :
 */
@SpringBootApplication
public class DevK3sApplication {
    public static void main(String[] args) {
        SpringApplication.run(DevK3sApplication.class, args);
    }
}
